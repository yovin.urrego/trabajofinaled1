/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.Pelicula;

import java.util.Date;

/**
 *
 * @author Yovin
 */
public class Pelicula {

    private String Title;
    private int Year;
    private String Rated;
    private String Released;
    private int Runtime;
    private String Genre;
    private String Director;
    private String Writer;
    private String Actors;
    private String Plot;
    private String Language;
    private String Country;
    private String Awards;
    private String Poster;
    private double[] Ratings = new double[3];
    private int Metascore;
    private double imdbRating;
    private double imdbVotes;
    private String imdbID;
    private String Type = "movie";
    private String DVD;
    private double BoxOffice;
    private String Production;
    private String Website;
    private boolean Response = true;

    public Pelicula(String Title, int Year, String Rated, String Released, int Runtime, String Genre, String Director, String Writer, String Actors, String Plot, String Language, String Country, String Awards, String Poster, int Metascore, double imdbRating, double imdbVotes, String imdbID, String DVD, double BoxOffice, String Production, String Website, double[]reg) {
        this.Title = Title;
        this.Year = Year;
        this.Rated = Rated;
        this.Released = Released;
        this.Runtime = Runtime;
        this.Genre = Genre;
        this.Director = Director;
        this.Writer = Writer;
        this.Actors = Actors;
        this.Plot = Plot;
        this.Language = Language;
        this.Country = Country;
        this.Awards = Awards;
        this.Poster = Poster;
        this.Metascore = Metascore;
        this.imdbRating = imdbRating;
        this.imdbVotes = imdbVotes;
        this.imdbID = imdbID;
        this.DVD = DVD;
        this.BoxOffice = BoxOffice;
        this.Production = Production;
        this.Website = Website;
        this.Ratings = reg;
    }

    /**
     * @return the Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param Title the Title to set
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return the Year
     */
    public int getYear() {
        return Year;
    }

    /**
     * @param Year the Year to set
     */
    public void setYear(short Year) {
        this.Year = Year;
    }

    /**
     * @return the Rated
     */
    public String getRated() {
        return Rated;
    }

    /**
     * @param Rated the Rated to set
     */
    public void setRated(String Rated) {
        this.Rated = Rated;
    }

    /**
     * @return the Released
     */
    public String getReleased() {
        return Released;
    }

    /**
     * @param Released the Released to set
     */
    public void setReleased(String Released) {
        this.Released = Released;
    }

    /**
     * @return the Runtime
     */
    public int getRuntime() {
        return Runtime;
    }

    /**
     * @param Runtime the Runtime to set
     */
    public void setRuntime(int Runtime) {
        this.Runtime = Runtime;
    }

    /**
     * @return the Genre
     */
    public String getGenre() {
        return Genre;
    }

    /**
     * @param Genre the Genre to set
     */
    public void setGenre(String Genre) {
        this.Genre = Genre;
    }

    /**
     * @return the Director
     */
    public String getDirector() {
        return Director;
    }

    /**
     * @param Director the Director to set
     */
    public void setDirector(String Director) {
        this.Director = Director;
    }

    /**
     * @return the Writer
     */
    public String getWriter() {
        return Writer;
    }

    /**
     * @param Writer the Writer to set
     */
    public void setWriter(String Writer) {
        this.Writer = Writer;
    }

    /**
     * @return the Actors
     */
    public String getActors() {
        return Actors;
    }

    /**
     * @param Actors the Actors to set
     */
    public void setActors(String Actors) {
        this.Actors = Actors;
    }

    /**
     * @return the Plot
     */
    public String getPlot() {
        return Plot;
    }

    /**
     * @param Plot the Plot to set
     */
    public void setPlot(String Plot) {
        this.Plot = Plot;
    }

    /**
     * @return the Language
     */
    public String getLanguage() {
        return Language;
    }

    /**
     * @param Language the Language to set
     */
    public void setLanguage(String Language) {
        this.Language = Language;
    }

    /**
     * @return the Country
     */
    public String getCountry() {
        return Country;
    }

    /**
     * @param Country the Country to set
     */
    public void setCountry(String Country) {
        this.Country = Country;
    }

    /**
     * @return the Awards
     */
    public String getAwards() {
        return Awards;
    }

    /**
     * @param Awards the Awards to set
     */
    public void setAwards(String Awards) {
        this.Awards = Awards;
    }

    /**
     * @return the Poster
     */
    public String getPoster() {
        return Poster;
    }

    /**
     * @param Poster the Poster to set
     */
    public void setPoster(String Poster) {
        this.Poster = Poster;
    }

    /**
     * @return the Ratings
     */
    public double[] getRatings() {
        return Ratings;
    }

    /**
     * @param Ratings the Ratings to set
     */
    public void setRatings(double[] Ratings) {
        this.Ratings = Ratings;
    }

    /**
     * @return the Metascore
     */
    public int getMetascore() {
        return Metascore;
    }

    /**
     * @param Metascore the Metascore to set
     */
    public void setMetascore(int Metascore) {
        this.Metascore = Metascore;
    }

    /**
     * @return the imdbRating
     */
    public double getImdbRating() {
        return imdbRating;
    }

    /**
     * @param imdbRating the imdbRating to set
     */
    public void setImdbRating(double imdbRating) {
        this.imdbRating = imdbRating;
    }

    /**
     * @return the imdbVotes
     */
    public double getImdbVotes() {
        return imdbVotes;
    }

    /**
     * @param imdbVotes the imdbVotes to set
     */
    public void setImdbVotes(double imdbVotes) {
        this.imdbVotes = imdbVotes;
    }

    /**
     * @return the imdbID
     */
    public String getImdbID() {
        return imdbID;
    }

    /**
     * @param imdbID the imdbID to set
     */
    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    /**
     * @return the Type
     */
    public String getType() {
        return Type;
    }

    /**
     * @param Type the Type to set
     */
    public void setType(String Type) {
        this.Type = Type;
    }

    /**
     * @return the DVD
     */
    public String getDVD() {
        return DVD;
    }

    /**
     * @param DVD the DVD to set
     */
    public void setDVD(String DVD) {
        this.DVD = DVD;
    }

    /**
     * @return the BoxOffice
     */
    public double getBoxOffice() {
        return BoxOffice;
    }

    /**
     * @param BoxOffice the BoxOffice to set
     */
    public void setBoxOffice(double BoxOffice) {
        this.BoxOffice = BoxOffice;
    }

    /**
     * @return the Production
     */
    public String getProduction() {
        return Production;
    }

    /**
     * @param Production the Production to set
     */
    public void setProduction(String Production) {
        this.Production = Production;
    }

    /**
     * @return the Website
     */
    public String getWebsite() {
        return Website;
    }

    /**
     * @param Website the Website to set
     */
    public void setWebsite(String Website) {
        this.Website = Website;
    }

    /**
     * @return the Response
     */
    public boolean isResponse() {
        return Response;
    }

    /**
     * @param Response the Response to set
     */
    public void setResponse(boolean Response) {
        this.Response = Response;
    }
}
