/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ColaPersonas;

import controlador.TDA.ColaServices;
import javax.swing.table.DefaultTableModel;
import modelo.Persona.Persona;
import modelo.Ticket.Ticket;

/**
 *
 * @author Yovin
 */
public class ColaPersonas {

    private ColaServices<Ticket> colaPersonas = new ColaServices(20);
    private int contador = 0;

    /**
     * @return the colaPersonas
     */
    public ColaServices getColaPersonas() {
        return colaPersonas;
    }

    /**
     * @param colaPersonas the colaPersonas to set
     */
    public void setColaPersonas(ColaServices colaPersonas) {
        this.colaPersonas = colaPersonas;
    }

    public void agregar(String nombre, String asunto) {
        Persona p = new Persona(nombre);
        Ticket tA = new Ticket(asunto, contador, p);
        colaPersonas.agregar(tA, true);
        contador++;
    }

    public DefaultTableModel getTable() {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Id ticket");
        model.addColumn("Nombre");
        model.addColumn("Asunto");
        String datos[] = new String[3];
        for (int i = 0; i < colaPersonas.getTamanioT(); i++) {
            datos[0] = String.valueOf(colaPersonas.obtenerDato(i ).getNroTicket()+1);
            datos[1] = colaPersonas.obtenerDato(i).getP().getNombre();
            datos[2] = colaPersonas.obtenerDato(i ).getAsunto();
            model.addRow(datos);
        }
        return model;
    }

}
