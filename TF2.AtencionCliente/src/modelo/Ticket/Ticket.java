/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.Ticket;

import modelo.Persona.Persona;

/**
 *
 * @author Yovin
 */
public class Ticket {
    private String asunto;
    private int nroTicket;
    private Persona p;

    /**
     * @return the asunto
     */
    public String getAsunto() {
        return asunto;
    }

    /**
     * @param asunto the asunto to set
     */
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    /**
     * @return the nroTicket
     */
    public int getNroTicket() {
        return nroTicket;
    }

    /**
     * @param nroTicket the nroTicket to set
     */
    public void setNroTicket(int nroTicket) {
        this.nroTicket = nroTicket;
    }

    /**
     * @return the p
     */
    public Persona getP() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setP(Persona p) {
        this.p = p;
    }

    public Ticket(String asunto, int nroTicket, Persona p) {
        this.asunto = asunto;
        this.nroTicket = nroTicket;
        this.p = p;
    }
}
