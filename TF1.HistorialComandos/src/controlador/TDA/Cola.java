/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.TDA;

import controlador.TDA.exception.PosicionException;
import controlador.TDA.exception.TopeException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yovin
 */
public class Cola<E> {

    private ListaEnlazadaServices lista = new ListaEnlazadaServices();
    private Integer tamanioT;

    public Cola(Integer tamanioT) {
        this.tamanioT = tamanioT;
    }

    public void agregar(E dato, boolean eliminar) throws TopeException {
        if (lista.getSize() < tamanioT) {
            this.lista.insertarAlInicio(dato);

        } else {
            if (eliminar) {
                this.lista.eliminarUltimo();
                this.lista.insertarAlInicio(dato);
            } else {
                throw new TopeException("Error pila llena");
            }
        }

    }

    public E obtenerDato(Integer pos) {
        return (E) lista.obtenerDato(pos);
    }

    public void listar() {
        this.lista.listar();
    }

    /*  
    public static void main(String[] args) {
        try {
            Cola a = new Cola(3);
            a.agregar("Gato", true);
             a.agregar("Saco", true);
              a.agregar("Paco", true);
               a.agregar("Caco", true);
               a.agregar("Trato", false);
              a.listar();
        } catch (TopeException ex) {
            Logger.getLogger(Cola.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
     */
    /**
     * @return the tamanioT
     */
    public Integer getTamanioT() {
        return lista.getSize();
    }

}
