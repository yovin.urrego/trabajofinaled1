/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.TDA;

import controlador.TDA.exception.TopeException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yovin
 */
public class ColaServices<E> {
    
    private Cola pilaService;
    
    public ColaServices(int tamanio) {
        pilaService = new Cola(tamanio);
    }
    
    public void agregar(E dato, boolean eliminar) {
        try {
            this.pilaService.agregar(dato, eliminar);
        } catch (TopeException ex) {
            System.out.println("Error");
        }
    }
    
    public void listar() {
        this.pilaService.listar();
    }

    public int tamanio() {
        return this.pilaService.getTamanioT();
    }
    
    public E obtenerDato(Integer pos) {
        return (E) this.pilaService.obtenerDato(pos);
    }
    
}
