/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.CtrlHistorial;

import modelo.HistorialCodigos.Code;
import modelo.HistorialCodigos.Historial;

/**
 *
 * @author Yovin
 */
public class CtrlHistorial extends Historial {

    public CtrlHistorial(int tamanio) {
        super(tamanio);
    }
    public void agregar(Code code){
       super.setPilaCode(code);
    }
    
     public void listar(){
         for (int i = 0; i < super.getPilaCode().tamanio(); i++) {
             System.out.println(super.obtenerDato(i).getCode()+"["+super.obtenerDato(i).getDate().getTime()+"]"); 
         }
    }
}
