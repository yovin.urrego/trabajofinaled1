/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.CtrlHistorial.Services;

import controlador.CtrlHistorial.CtrlHistorial;
import modelo.HistorialCodigos.Code;

/**
 *
 * @author Yovin
 */
public class CtrlHistorialServices {

  private CtrlHistorial servicio;

    public CtrlHistorialServices(int tamanio) {
        this.servicio =  new CtrlHistorial(tamanio);
    }
    public void agregar(Code code){
    this.servicio.agregar(code);
    }
    
    public void listar(){
    servicio.listar();
    }
  
}
