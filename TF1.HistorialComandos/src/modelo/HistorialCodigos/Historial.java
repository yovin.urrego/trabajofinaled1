/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.HistorialCodigos;

import controlador.TDA.ListaEnlazadaServices;
import controlador.TDA.ColaServices;

/**
 *
 * @author Yovin
 */
public class Historial {

    private ColaServices ColaCode;

    /**
     * @return the ColaCode
     */
    public ColaServices getPilaCode() {
        return ColaCode;
    }

    /**
     * @param pilaCode the ColaCode to set
     */
    public void setPilaCode(Code code) {
        this.ColaCode.agregar(code, true);
    }

    public int tamanio() {
        return this.getPilaCode().tamanio();
    }

    public Historial(int tamanio) {
        this.ColaCode = new ColaServices(tamanio);
    }

    /**
     * @param pilaCode the ColaCode to set
     */
    public void setPilaCode(ColaServices pilaCode) {
        this.ColaCode = pilaCode;
    }
     public Code obtenerDato(Integer pos) {
        return  (Code) this.ColaCode.obtenerDato(pos);
    }
    

}
