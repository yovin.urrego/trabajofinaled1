/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package vista.Historial;

import controlador.CtrlHistorial.Services.CtrlHistorialServices;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import modelo.HistorialCodigos.Code;

/**
 *
 * @author Yovin
 */
public class GuardarComandos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        CtrlHistorialServices historial = new CtrlHistorialServices(10);
        // TODO code application logic here
        System.out.println("***************************************************");
        System.out.println("****************Consola de comandos****************");
        System.out.println("***************************************************");
        System.out.println("**********Salir: EXIT -- Ver Comandos: VC**********");
        System.out.println("*********Maximo 10 Comandos hasta eliminar*********");
        System.out.println("***************************************************");
        System.out.println("Ingresar Comandos:");
        String comando;

        do {
            comando = s.nextLine();
            Code code = new Code(comando);
            historial.agregar(code);
            System.out.println("**********************");
            System.out.println("******EJECUTANDO******");
            System.out.println("**********FIN*********");
            System.out.println("**********************");
            if (comando.equals("VC")) {
                historial.listar();
            }

        } while (comando.compareTo("EXIT") != 0);
        System.out.println("Saliendo...");

    }

}
